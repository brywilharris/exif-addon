var echo_exif = false;
var echo_links = false;
var echo_bigs = false;
"use strict";

$(document).ready(function(){
    //console.log(this.toString());
    //if (this.toString() === "[object HTMLDocument]"){
        //$("body").before('<style>img[title]:hover:after {content: attr(title);padding: 4px 8px;color: #333;position: absolute;left: 0;top: 100%;z-index: 20;white-space: nowrap;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px;-moz-box-shadow: 0px 0px 4px #222;-webkit-box-shadow: 0px 0px 4px #222;box-shadow: 0px 0px 4px #222;background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #eeeeee),color-stop(1, #cccccc));background-image: -webkit-linear-gradient(top, #eeeeee, #cccccc);background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);background-image: -ms-linear-gradient(top, #eeeeee, #cccccc);background-image: -o-linear-gradient(top, #eeeeee, #cccccc);}</style>');

        var index = 0;
        var element;

        $.each($("img"), function(index, value){
            if (this.getAttribute("exified") != "true"){ 
                this.onload = newimage(this);
                
        }});
        

        $(window).scroll(function (event) {
            var index = 0;
            $.each($("img"), function(index, value){
                //console.log("images: "+document.images.length);
                if (this.getAttribute("exified") != "true"){ 
                    this.onload = newimage(this);
                }
            });
        });
        
        $("img").mouseenter(function(){
            //console.log(this.src);
            
            if (this.getAttribute("linked") != "true"){
                this.setAttribute("linked", "true");
                var href = findhref(this, 5);
                getEXIFlink(href,this);
            }
            
        });
        
    //}

    if (this.toString() === "[object ImageDocument]"){
    //tbd
    }
});

function newimage(image){
    //console.log(image.src);
    var $this = $(image);
    try{
        var anchor = $this.closest("a");
        myhref = anchor.attr("href");
    }catch(err){}
    //console.log(myhref);
    image.setAttribute("exified", "true");
    var source = image.src;
    
    image.style.border = "1px dotted grey"
    var width = image.width;
    var height = image.height;


    if (width>150 && height>200){
        image.style.border = "2px solid green";
        image.bigimage = true;
        //console.log(jqthis);
//         jqthis.wrap("<div class='myexiftooltip' " // style='z-index: 1' "
//         + "id=my-exif></div>");
        if(echo_bigs){console.log("big picture: "+source );}
    }
    process_exif(image);
};

function findhref(image, levels){
    var i = 0;
    var href = null;
    try{
        myparent = image.parentNode;
        for(i=0;i<levels;i++){
            //console.log(myparent.id);
            href = myparent.href;
            //console.log(href);
            if (href != null && href != undefined){
                //console.log(myparent.type);
                return href;   
            };
            myparent = myparent.parentNode;
        }
    }catch(error){
        console.log("Error: " + error);
        return error;
    }
};

function process_exif(image){
    EXIF.getData(image, function() {
        image.title = image.src.substring(0,100);
        try{
            //console.log(this.exifdata);
            var alltags = EXIF.getAllTags(this);
            //console.log(alltags);
            image.alltags = alltags;
            var make = EXIF.getTag(this, "Make");
            var model = EXIF.getTag(this, "Model");
            var date = EXIF.getTag(this, "DateTimeOriginal");
            var makemodel;
            if (model.beginsWith(make)){
                makemodel = model;
            }else{
                makemodel = make + " " + model;
            }
        }catch(err){
            if (err.name==='TypeError'){}
            else{
                console.log("Make-Model detection: "+err);
            }
        }
        try{
            var lat = EXIF.getTag(this, "GPSLatitude");
            lat = lat[0]+lat[1]/60+lat[2]/3600;
            var latref = EXIF.getTag(this, "GPSLatitudeRef");
            if (latref == "S"){lat = -lat};
            var latitude = lat.toString() + " (" +  latref + ")";
            var long = EXIF.getTag(this, "GPSLongitude");
            long = long[0]+long[1]/60+long[2]/3600;
            var longref = EXIF.getTag(this, "GPSLongitudeRef");
            if (longref != "E"){long = -long};
            var longitude = long.toString() + " (" +  longref + ")";
            
            if (latitude != null && echo_exif){console.log(latitude);}
            if (longitude != null && echo_exif){console.log(longitude);}
        }catch(err){
            if (err.name==='TypeError'){}
            else{
                console.log("Lat-Long detection: "+err);
            }
        }
        //Add info we have found to the title
        if (makemodel != undefined){/*image.title += '\n' + makemodel;*/image.style.border = "2px solid yellow";}
        if (date != undefined){/*image.title += '\n' + date;*/}
        if (lat != undefined){/*image.title += '\n' + latitude;*/image.style.border = "3px solid red";}
        if (long != undefined){/*image.title += '\n' + longitude;*/image.style.border = "3px solid red";}
        if (alltags!= undefined){
            for (each in alltags){
            image.title += "\n" + each + ": " + alltags[each];
            }
            
        }
        
    });
};

String.prototype.beginsWith = function (string) {
    return(this.indexOf(string) === 0);
};

String.prototype.endsWith = function (string) {
    return(this.indexOf(string) === (this.length - string.length));
};

function getEXIFlink(href, myimage) {
    var http = new XMLHttpRequest();
    
    http.open("GET", href, true);
    http.responseType = "blob";
    http.onload = function(e) {
        if (this.status === 200) {
            //var string64 = to64(http.response);
            //console.log(base64string);
            //var image = new Image();
            var image = new Image();
            image.onload = function() {
                //console.log("appending: ", image.src);
                //document.body.appendChild(this);
                this.width = 200;
                document.body.appendChild(this);
            };
            image.src = to64(http.response);
        }
    };
    http.send();
    return image;
}

function to64(blob){
    var reader = new window.FileReader();
    reader.readAsDataURL(blob); 
    reader.onloadend = function() {
        base64data = reader.result;                
        //console.log(base64data );
    }
    return base64data;
}
