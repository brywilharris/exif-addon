var self = require("sdk/self");
var pageMod = require("sdk/page-mod");
var ui = require("sdk/ui");
var notifications = require("sdk/notifications");

// var cm = require("sdk/context-menu").SelectorContext("img");

pageMod.PageMod({
    include: "*",
    contentScriptFile: ["./exif.js","./jquery-2.2.1.min.js","./readim.js"],
    //contentScript: 'window.alert("Matched!");',
    contentScriptWhen: "start"
});

var action_button = ui.ActionButton({
    id: "my-button",
    label: "Action Button!",
    icon: "./icon-48.png",
    onClick: function(state) {
        console.log("You clicked '" + state.label + "'");
    
    }
});

// cm.Item({
//   label: "My Menu Item",
//   context: cm.URLContext("*")
// });