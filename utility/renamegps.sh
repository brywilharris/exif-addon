for foo in *.JPG
do
	if [[ "$foo" != '*.JPG' ]]
	then
		mv "$foo" "${foo/.JPG/.jpg}"
	fi
done

for foo in *.JPEG
do
	if [[ "$foo" != '*.JPEG' ]]
	then
		mv "$foo" "${foo/.JPEG/.jpg}"
	fi
done

for foo in *.jpeg
do
	if [[ "$foo" != '*.jpeg' ]]
	then
		mv "$foo" "${foo/.jpeg/.jpg}"
	fi
done

for foo in *.jpg;
do
fn=$foo;
temp1=`exifprobe -L $fn|grep -E '(Longitude)'|cut -f 2 -d '='|grep -v TIFF|tr -d "\n'"|tr ',' '_'|tr ' ' '_'` ;
temp1=${temp1/\\000/}
temp1=${temp1/_/}

if [[ $temp1 == *'_W' ]]
then
	temp1="W_${temp1/_W/}"
fi

if [[ $temp1 == *'_E' ]]
then
	temp1="E_${temp1/_E/}"
fi

temp2=`exifprobe -L $fn|grep -E '(Latitude)'|cut -f 2 -d '='|grep -v TIFF|tr -d "\n'"|tr ',' '_'|tr ' ' '_'` ;
temp=${temp1}${temp2}_

if [[ $fn != *${temp2}* ]]
then
	echo $fn '->' $temp$fn
	mv "$fn" "$temp$fn"
fi
done

for foo in W_?_*; 
do 
	if [[ $foo != 'W_?_*' ]]
	then
		mv $foo ${foo/W_/W_0}; 
	fi
done

for foo in W_??_*; 
do 
	if [[ $foo != 'W_??_*' ]]
	then
		mv $foo ${foo/W_/W_0};
	fi	
done

for foo in E_?_*; 
do 
	if [[ $foo != 'E_?_*' ]]
	then
		mv $foo ${foo/E_/E_0}; 
	fi
done

for foo in E_??_*; 
do 
	if [[ $foo != 'E_??_*' ]]
	then
		mv $foo ${foo/E_/E_0};
	fi
done

#rm E_000* W_000* 'E\000_0_0_0_N_0_0_0_'*
