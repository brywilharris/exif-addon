
from PIL import Image
from PIL import ExifTags

import sys

def printinfo(filename):
   myimage = Image.open(filename)
   print("")
   print("")
   #myimage.verify()
   myimage.load()
   print (filename)
   print (myimage.info.keys())
   exif = {
      ExifTags.TAGS[k]: v
      for k, v in myimage._getexif().items()
      if k in ExifTags.TAGS
   }
   for each in exif.keys():
      print (str(each)+" : "+str(exif[each]))
   
def printgps(filename):
   myimage = Image.open(filename)
   print("")
   print("")
   myimage.load()
   print (filename)
   try:
      exif = {
         ExifTags.TAGS[k]: v
         for k, v in myimage._getexif().items()
         if k in ExifTags.TAGS
      }
      #print exif.keys()
   except:
      print "Error getting exif."
   gps = []
   
   try:
      gps = [x for x in exif.keys() if x == 'GPSInfo']
   except:
      print "No GPS."
   for each in gps:
      print "GPS:"
      print (str(each)+" : "+str(exif[each]))


#printinfo('content_example_ibiza.jpg')
#printinfo('img_1771.jpg')
#printinfo('Lenna.png')

printgps('content_example_ibiza.jpg')
printgps('img_1771.jpg')
printgps('Lenna.png')

import os
directories = os.walk('.') #if str(each).endswith(".png")]
print [each[2] for each in directories]
