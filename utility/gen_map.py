#!/usr/bin/python
from __future__ import print_function
import os
import sys
from PIL import Image
from PIL.ExifTags import TAGS
import urllib2
import ast
#import pyexiv2
import reverse_geocoder as rg

size = (400, 400)
picsperpage = 25


def head(title, js):
    string = '''
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="mystyle.css">
<title>
'''
    string += title
    string += '''
</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"
type="text/javascript"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDkHRspR2X2xDbbqwEdqK_iJj1_29Yru0Y"
type="text/javascript"></script>
<script src="'''
    string += js
    string += '''" type="text/javascript"></script>
</head>
<body>
'''
    return string


def img(path, alt, mysize):
    if not os.path.exists("thumbs"):
        os.makedirs("thumbs")
    infile = path
    print(os.path.splitext(infile)[0].split('/')[-1])
    if len(os.path.splitext(infile)[0].split('/')) > 2:
        outfile = "thumbs/" + os.path.splitext(infile)[0].split('/')[-2] \
        + "_" + os.path.splitext(infile)[0].split('/')[-1] + ".thumbnail"
    else:
        outfile = "thumbs/" + os.path.splitext(infile)[0].split('/')[-1] \
        + ".thumbnail"
    if not os.path.isfile(outfile):
        im = Image.open(infile)
        try:
            exif = im.info['exif']
            im.thumbnail(mysize, Image.ANTIALIAS)
            im.save(outfile, "JPEG", exif=exif)
            #print("Got past the exif part")
        except:
            print("Creating without exif")
            im.thumbnail(mysize, Image.ANTIALIAS)
            im.save(outfile, "JPEG")
    imgsrc = '<a href="' + infile + '">\n<img src="' + outfile
    imgsrc += '" \nalt="' + alt + '" />\n</a>\n'
    return imgsrc


def exifgps(myimage):
    (lat, lon) = (0, 0)
    try:
        for (k, v) in list(Image.open(myimage)._getexif().items()):
            if TAGS.get(k) == 'GPSInfo':
                (lat, lon) = gpsdec(v)
    except:
        print("No GPS!")
    return (lat, lon)


def maplnk(each, text):
    try:
        (lat, lon) = exifgps(each)
        '''for (k,v) in Image.open(each)._getexif().iteritems():
            if TAGS.get(k) == 'GPSInfo':
                (lat,lon) = gpsdec(v)'''
        maplink = '<a href="https://maps.google.com/maps?q='
        maplink += str(lat) + ',' + str(lon)
        maplink += '">\n' + text + str(lat) + ',' + str(lon) + '\n</a>\n'
        if lat == 0 or lon == 0:
            return 'Empty GPS coordinates'
        return maplink
    except:
        return 'No ' + text + ' Link <br>\n'

def allexif(each, text):
    exifinfo = ""
    exifinfo += '<br>\n ' + text + '<br>\n'
    print(each, file=sys.stderr)
    im = Image.open(each)
    print(str(im.size[0]) + 'x' + str(im.size[1]), file=sys.stderr)
    exifinfo += str(im.size[0]) + 'x' + str(im.size[1]) + '<br>\n'

    # print(im.info, file=sys.stderr)
    try:
        mydict = im._getexif()
    except:
        print("No exif", file=sys.stderr)
        exifinfo += "No exif <br>\n"
        return
    try:
        print(mydict[271] + ' ' + mydict[272], file=sys.stderr)
        exifinfo += mydict[271] + ' ' + mydict[272] + ' <br>\n'
    except:
        print("Unknown camera", file=sys.stderr)
        exifinfo += "Unknown camera" + ' <br>\n'
    try:
        print(mydict[306], file=sys.stderr)
        exifinfo += mydict[306] + ' <br>\n'
    except:
        print("Unknown date", file=sys.stderr)
        exifinfo += "Unknown date" + ' <br>\n'
    return exifinfo


def osmhref(lat, lon):
    thishref = "http://open.mapquestapi.com/nominatim/v1/reverse.php?"\
    + "key=alhDjcIJ9egBKDVgqG9jw3PLBOKSExaL&format=json&lat="
    thishref += str(lat) + '&lon=' + str(lon)
    #"http://open.mapquestapi.com/geocoding/v1/reverse?"\
    #+"key=YOUR_KEY_HERE&callback=renderReverse"\
    #+"&json={location:{latLng:{lat:40.053116,lng:-76.313603}}}"
    return thishref

def osmlnk(each, text):
    try:
        lat = 0
        lon = 0
        for (k, v) in list(Image.open(each)._getexif().items()):
            if TAGS.get(k) == 'GPSInfo':
                (lat, lon) = gpsdec(v)
        maplink = '<a href="' + osmhref(lat, lon) + '">\n'
        maplink += text
        maplink += '</a>\n'
        return maplink
    except:
        return 'No ' + text + ' Link \n'


def fetch_osmlnk(each):
    try:
        lat = 0
        lon = 0
        for (k, v) in list(Image.open(each)._getexif().items()):
            if TAGS.get(k) == 'GPSInfo':
                (lat, lon) = gpsdec(v)
        info = urllib2.urlopen(osmhref(lat, lon)).read()
        try:
            address = ast.literal_eval(info)['display_name']
            return info, address
        except:
            return info, ""
    except:
        return '"Oops, no href found"', '"No address available"'


def googleme(term):
    url = 'https://www.google.com/#q='
    term = term.replace(' ', '+')
    term = term.replace(',', '')
    #print(term)
    url += term
    return url


def getfilesrecursive(path, extensions):
    'Gets a recursive list of files in the path with the [extensions]'
    mylist = os.walk(path)
    recursivelist = []
    for root, dirs, files in mylist:
        path = root.split('/')
        for file in files:
            (name, ext) = os.path.splitext(file)
            if ext.lower() in extensions:
                newentry = os.path.join(root, file)
                recursivelist += [newentry]
    return recursivelist


def makedarkcss():
    f = open('mystyle.css', 'w')
    f.write(
    '''
    body {
        background-color: black;
        color: hsl(120,100%,75%);
    }
    h1 {
        color: navy;
        margin: 20px;
    }
     .next {
        text-align: right;
        font-size: 2.0em;
    }
    .prev {
        text-align: left;
        font-size: 2.0em;
    }
    table {

    }
    td {
        vertical-align: top;
        text-align: left;
    }
    /* unvisited link */
    a:link {
        color: #FFFFFF;
    }

    /* visited link */
    a:visited {
        color: #00FF00;
    }

    /* mouse over link */
    a:hover {latlng
        color: #FF00FF;
    }

    /* selected link */
    a:active {
        color: #0000FF;
    }
    #map_div{
        width:r780px;
        height:580px;
        border:6px solid #F4F4F4;
        margin:20px auto 0 auto;
    }
    ''')
    f.close()


def gpsdec(v):
    lat = v[2][0][0] / v[2][0][1]
    lat += float(v[2][1][0]) / v[2][1][1] / 60
    lat += float(v[2][2][0]) / v[2][2][1] / 3600
    lon = v[4][0][0] / v[2][0][1]
    lon += float(v[4][1][0]) / v[2][1][1] / 60
    lon += float(v[4][2][0]) / v[2][2][1] / 3600
    if v[3] == 'W':
        lon = -lon
    if lat == 0 and lon == 0:
        raise(TypeError)
        return (0, 0)
    else:
        return (lat, lon)


def map_pins(locations):
    top = """
$(document).ready(function() {
var latlng = new google.maps.LatLng(39.68257777777778,-95);
var options = {
zoom: 5,
// This number can be set to define the initial zoom level of the map
center: latlng,
mapTypeId: google.maps.MapTypeId.ROADMAP
// This value can be set to define the map type ROADMAP/SATELLITE/HYBRID/TERRAIN
};
// Calling the constructor, thereby initializing the map
var map = new google.maps.Map(
document.getElementById("map_div"), options);
// Define Marker properties
//var image = new google.maps.MarkerImage("images/marker.png",
// This marker is 129 pixels wide by 42 pixels tall.
// new google.maps.Size(129, 42),
// The origin for this image is 0,0.
// new google.maps.Point(0,0),
// The anchor for this image is the base of the flagpole at 18,42.
// new google.maps.Point(18, 42));
// Add Marker
"""
    markerstart = """ = new google.maps.Marker({
position: new google.maps.LatLng
"""

    home = (39.682577777777778, -83.80890277777777)
    markerend = """,
map: map		  
});
"""
    bottom = """
});"""
    doc = top
    doc += "var home"
    doc += markerstart
    doc += str(home)
    doc += markerend
    i = 0
    center = str(home)
    for each in locations:
        i += 1
        doc += "var marker"
        doc += str(i)
        doc += markerstart
        doc += str(each)
        doc += markerend
        center = "marker"+str(i)
    doc += "map.setCenter("+center+".getPosition());"
    doc += bottom 
    return doc

''' var marker2 = new google.maps.Marker({
position: new google.maps.LatLng(45,-95),
map: map});
// Add listener for a click on the pin
google.maps.event.addListener(marker1, 'click', function() {
infowindow1.open(map, marker1);
});	  
// Add information window
var infowindow1 = new google.maps.InfoWindow({
content:  createInfo('Home')
});
// Create information window
function createInfo(title, content) {
return '<div class="infowindow"><strong>'
+ title +'</strong><br />'+content+'</div>';
}
});'''

def map_table(prev_file, curr_file, new_file):
    header = '<table width="90%">\n<tr>'
    if prev_file != '':
        header += '<td><div class=prev> <a href=' \
        + prev_file + '>BACK</a> </div></td>\n'
    else:
        header += '<td>&nbsp;</td>'
    if new_file != '':
        header += '<td><div class=next> <a href=' \
        + new_file + '>NEXT</a> </div></td>\n'
    else:
        header += '<td>&nbsp;</td>'
    header += '''</tr>
    <tr><td width="800">
    <div id="map_div">&nbsp;</div></td></tr>
    </table>
    <table>'''
    return header
    
def footer_table(prev_file, curr_file, new_file):
    footer = '</table><table width="90%">\n<tr>'
    if prev_file != '':
        footer += '<td><div class=prev> <a href=' \
        + prev_file + '>BACK</a> </div></td>\n'
    else:
        footer += '<td>&nbsp;</td>'
    if new_file != '':
        footer += '<td><div class=next> <a href=' \
        + new_file + '>NEXT</a> </div></td>\n'
    else:
        footer += '<td>&nbsp;</td>'
    footer += '</tr></table>'
    return footer

if __name__ == "__main__":
    #print(map_pins([(40,-80),(41,-81),(42,-82),
    #    (43,-83),(44,-84)]))
    makedarkcss()  #Make the format page
    recursivelist = getfilesrecursive(".", [".jpg", ".jpeg", ".JPG", ".JPEG"])
    filecount = 0
    index = 'index'
    ext = '.html'
    curr_file = index + ext
    locations = []
    g = open(index + ".js", 'w')  #Javascript map file 
    f = open(index + ext, 'w')  #html file
    #Insert header into first html file
    f.write(head("Picture Pages With Google Map", "index.js"))
    
    filenames = recursivelist
    filenames.sort()

    count = 0  #Count within page
    count2 = 0  #Running total
    pagecount = 1
    new_file = ''
    prev_file = ''
    dircount = 0
    mydir = filenames[0].split('/')[:-1]
    lastdir = mydir
    for each in filenames:
        mydir = each.split('/')[:-1]
        if mydir != lastdir:
            dircount += 1
        lastdir = mydir
    if len(filenames) > picsperpage or dircount > 1:
        new_file = index + '2.html'
    f.write(map_table(prev_file, curr_file, new_file))
    mydir = filenames[0].split('/')[:-1]

    for each in filenames:
        lastdir = mydir
        mydir = each.split('/')[:-1]
        count2 += 1
        print (str(count2) + '/' + str(len(filenames)), file=sys.stderr)
        #Here is where we handle page splitting
        if count >= picsperpage or mydir != lastdir:  
            pagecount += 1            
            new_map = index + str(pagecount) + '.js'
            #print ((prev_file, new_file))
            f.write('</table>\n')
            if pagecount == 2:  # First page
                f.write(footer_table('', curr_file, new_file))
            else:  # All but last page
                print ((prev_file, new_file))
                f.write(footer_table(prev_file, curr_file, new_file))
            f.close()
            g.write(map_pins(locations))
            g.close()
            locations = []
            # Above here goes in old file
            
            # Below goes in new file
            f = open(new_file, 'w')
            prev_file = curr_file
            curr_file = new_file
            g = open(new_map, 'w')
            new_file = index + str(pagecount + 1) + '.html'
            f.write(head("Picture Pages, Picture Pages "
            + str(pagecount), new_map))
            # Write Back and Next Buttons at top of page
            if len(filenames) > count2:  # If not last page
                f.write(map_table(prev_file, curr_file, new_file))
            else:  # If last page
                f.write(map_table(prev_file, curr_file, ''))
            count = 0
        locations += [exifgps(each)]
        (name, ext) = os.path.splitext(each)
        eachpath = os.path.relpath(each)
        print(eachpath)
        if ext.lower() in [".jpg", ".jpeg"]:
            f.write('<tr>\n<td width="' + str(size[0]) + '">\n')
            try:
                f.write(img(eachpath, each, size))
            except(IOError):
                print("Error writing file for " + eachpath , file=sys.stderr)
            #f.write('<br>\n'+name+ext+'\n')
            f.write('</td>\n')
            f.write('<td width="' + str(size[0] * 0.6) + '">\n')
            try:
                f.write(allexif(each, eachpath).encode('ascii', 'ignore'))
            except:
                print("Error writing exif for " + eachpath , file=sys.stderr)
            f.write(maplnk(each, "Map ") + '<br>')
            
            print(each, file=sys.stderr)
            try:
               for (k, v) in list(Image.open(each)._getexif().items()):
                  if TAGS.get(k) == 'GPSInfo':
                     (lat, lon) = gpsdec(v)
               results = rg.search((lat,lon))
               city = results[0]['name']
               country = results[0]['cc']
               county = results[0]['admin2']
               state = results[0]['admin1']
               address = city + " " + county + "\n" +state + " " + country + " "
               print(address, file=sys.stderr)
               address = city + " " + county + "<br>\n" +state + " " + country + " "
               f.write(address + '<br>\n')
            except:
                print(each, file=sys.stderr)
                print("Has no readable GPS info")
                f.write('Unknown address <br>\n')

            #This calls a mapquest API which is running out of transactions.  Skip for now.
            #(info, addr) = fetch_osmlnk(each)
            #print(info, file=sys.stderr)
            #print(addr, file=sys.stderr)
            #google_link = '<a href='
            #google_link += googleme(addr)
            #google_link += '>' + "Google This Address"
            #google_link += '</a><br>\n'
            #f.write(google_link)

            #f.write(osmlnk(each, "Open Street Maps Link") + '<br>\n')
            #f.write(addr + '<br>\n')

            f.write('</td>\n</tr>\n')

            count += 1
    f.write('</table>\n')
    f.write('</body>')
    f.close()
    g.write(map_pins(locations))
    g.close()


#Google example code??
#maplink = width="'+size[0]+'"><iframe width="'+size[0]\
#+'" height="'+size[1]+'" frameborder="0" \n'
#maplink += 'scrolling="no" marginheight="0" marginwidth="0" \n'
#maplink += 'src="https://maps.google.com/maps?q='
#maplink += str(lat)+','+str(lon)
#maplink += '\n&amp;ie=UTF8&amp;ll='
#maplink += str(lat)+','+str(lon)
#maplink += '\n&amp;t=h&amp;z=25&amp;output=embed"></iframe><br />\n'
#maplink += '\n&amp;t=m&amp;z=25&amp;output=embed"></iframe><br />\n'
#'<td width="'+str(size[0])+'">UNABLE TO RETREIVE MAP</td></tr>\n'

